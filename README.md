Prototype version. Please bookmark and check back for a release version early next year.
See google doc for more info:
https://docs.google.com/document/d/1dN9Q989IBA2Q7qFCOlIc_CYo9b8OKhsJPxvwOKoSWuQ/edit#

### Install

Install and load `devtools`:

```
install.packages("devtools")
library(devtools)
```

Install `salmonfisher` from gitlab:

```
install_git('https://gitlab.com/sandve-lab/salmonfisher')
library(salmonfisher)
```

Required packages:  
```
library(DBI)
library(RSQLite)

# Or library(tidyverse) to load all below
library(tidyr)
library(dplyr)
library(dbplyr)
library(stringr)
```

