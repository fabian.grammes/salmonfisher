% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{fish_species_short_name}
\alias{fish_species_short_name}
\title{Convert to short species code name}
\usage{
fish_species_short_name(name)
}
\description{
Given an acceptable species name, return the relevant short species code name.
}
