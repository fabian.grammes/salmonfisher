---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Install

Install and load `devtools`:

```r
install.packages("devtools")
library(devtools)
```

Install `salmonfisher` from gitlab:

```r
install_git('https://gitlab.com/sandve-lab/salmonfisher')
library(salmonfisher)
```
