---
layout: page
title: Jekyll notes
permalink: /jekyll/
---

# Notes on how to 
1. Get [jekyll](https://jekyllrb.com/) up and running on a mac
2. Run the continous integration with GitLab
3. Structure the `doc` folder


## Install `jekyll` on mac 
   
Nice to have a local copy to test the page geenration and look on the fly. However, installing jekyy was a bit painfull. I started with lokking for a conda environment - and bingo there we have [one](https://anaconda.org/conda-forge/rb-jekyll) 

```sh
# make a new environment 
conda create --name jekyll
conda activate jekyll
# and insatll
conda install -c conda-forge rb-jekyll 
```

However, unfortunatly that 'only' installs `jekyll` and not `bundler` which is reccomended. There is a [bundler](https://anaconda.org/conda-forge/rb-bundler) package on conda, but that does not communicate with jekyll when insatlled in the same conda environment. 

Also trying to install `ruby` packaged in the conda environment via `gem insatll ...` generally works, but not for jekyll or bundler. 

So then I finally settled on installing `jekyll` with `brew` which is the reccomended way - but I stroingly dislike brew, since it always ends up installing a lot of extra stuff in my home environment. 


```sh
brew install ruby
echo 'export PATH="/usr/local/opt/ruby/bin:$PATH"' >> ~/.bash_profile
which ruby
# /usr/local/opt/ruby/bin/ruby
ruby -v
# ruby 2.7.1p83 (2020-03-31 revision a0c7c23c9c) [x86_64-darwin19]

# Install
gem install --user-install bundler jekyll
# may have been neccessary to do a sudo gem install ...
```

## Use GitLab to serve the site

### jekyll theme

in order for GitLab pages (or GitHub for that matter) to correctly render the page do the following (see https://github.com/jekyll/jekyll/issues/6712) exsample for the `minima` theme

```sh
git clone https://github.com/jekyll/minima
jekyll new testsite
cd minima
cp -R _includes _layouts _sass assets ../testsite
cd ../testsite
vi _config.yml # add jekyll-seo-tag and remove theme
bundle update
bundle exec jekyll build
```

### `_config.yml`

Jekyll configurations contained in `_config.yml`

```yml
title: AquaGen tools
email: fabian.grammes@aquagen.no
description: >- # this means to ignore newlines until "baseurl:"
  Write an awesome description for your new site here. You can edit this
  line in _config.yml. It will appear in your document head meta (for
  Google search results) and in your feed.xml site description.
baseurl: "/conda/aqg_tools" # the subpath of your site, e.g. /blog
url: "https://FabianGrammes.gitlab.io" # the base hostname & protocol for your site, e.g. http://example.com
twitter_username: jekyllrb
github_username:  jekyll

# Build settings
markdown: kramdown
theme: minima
plugins:
  - jekyll-feed
```

Especially important are the 2 lines:

- `baseurl: "/conda/aqg_tools" # the subpath of your site, e.g. /blog` which points to the directory within GitLab
- `url: "https://FabianGrammes.gitlab.io"` which points to the Group/User repo see https://gitlab.com/pages/jekyll/-/issues/29#note_265175544

### `.gitlab-ci.yml` 

Using GitLab [CI](https://docs.gitlab.com/ee/ci/) to serve the page. The `.gitlab-ci.yml` file specifies everything neccessary to execute the CI on a public runner. The `.gitlab-ci.yml` part to serve a satic `jekyll` page looks like this: 

```yml
image: ruby:latest

variables:
  JEKYLL_ENV: production
  LC_ALL: C.UTF-8

before_script:
  - gem install bundler
  - cd doc
  - bundle install

pages:
  stage: deploy
  script:
  - bundle exec jekyll build doc -d public
  - cp -r public ../
  artifacts:
    paths:
    - public
  ```

So step by step - also the [CI yml ref](https://docs.gitlab.com/ee/ci/yaml/)

- `image` tells the CI to use the latest ruby docker image
- `variables` not sure
- `before_script` Set of commads executed before running the actual CI script
- `pages` The script that generates the `jekyll` pages and will be served through GitLab pages.
  - `script` the command that excutes `jekyll` and saves everything in a folder called `doc/public` (since we did a `cd doc` in the before_script). So this has to be cp/mv to the root before it can be declared as `artifact` 
  - (see [artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)). In short artifacts defines a folder that is attached to the commit CI, the convention is that a folder called public will be served through Gitlab pages. 


